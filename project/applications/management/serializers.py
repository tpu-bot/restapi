from rest_framework import serializers
from management.models import (
    Broadcast,
)


class BroadcastSerializer(serializers.ModelSerializer):

    class Meta:
        model = Broadcast
        exclude = ('id', 'status')

