from django.db import models


class Broadcast(models.Model):
    STATUS_CREATED = 'created'
    STATUS_COMPLETED = 'completed'
    _STATUSES = (
        (STATUS_CREATED, 'Создан'),
        (STATUS_COMPLETED, 'Завершён'),
    )
    message = models.TextField(
        verbose_name='Сообщение', null=False, blank=False)
    user_selection = models.TextField(
        verbose_name='Выборка пользователей', null=False, blank=False)
    created_at = models.DateTimeField(
        db_index=True,
        auto_now_add=True,
        verbose_name='Время создания',
    )
    status = models.CharField(
        verbose_name='Статус',
        max_length=100,
        choices=_STATUSES,
        default=STATUS_CREATED
    )

    def __str__(self):
        return f"Бродкаст {self.message}"

    class Meta:
        verbose_name = 'Бродкаст'
        verbose_name_plural = 'Бродкасты'
