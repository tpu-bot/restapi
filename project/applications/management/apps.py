from django.apps import AppConfig


class ManagementConfig(AppConfig):
    name = 'management'
    verbose_name = 'Управление'

    def ready(self):
        import management.signals
