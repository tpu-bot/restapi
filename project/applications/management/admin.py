from django.contrib import admin

# Register your models here.
from management.models import Broadcast


class BroadcastAdmin(admin.ModelAdmin):
    list_display = ('user_selection', 'message', 'status',)


admin.site.register(Broadcast, BroadcastAdmin)
