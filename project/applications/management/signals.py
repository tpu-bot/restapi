from django.db.models.signals import post_save
from django.dispatch import receiver

from core.messaging import (
    create_new_broadcast_message,
    queue,
)
from .models import Broadcast


@receiver(post_save, sender=Broadcast, dispatch_uid='notify_new_broadcast')
def on_new_broadcast(sender, instance: Broadcast, created, *args, **kwargs):
    if created:
        # TODO: Check if no users - send to all
        message = create_new_broadcast_message(instance)
        queue.send_message(message)

        # TODO: Should it be changed on callback or something else?
        instance.status = Broadcast.STATUS_COMPLETED
        instance.save(update_fields=['status'])
