import datetime

import dryscrape
import requests
from bs4 import BeautifulSoup

BASE_URL = "https://rasp.tpu.ru"


def get_current_week_schedule(identifier):
    today = datetime.date.today()
    _, week_num, _ = today.isocalendar()
    tpu_week = week_num + 17  # TODO: Sync weeks
    dryscrape.start_xvfb()
    sess = dryscrape.Session()
    url = f"{BASE_URL}/{identifier}/2019/{tpu_week}/view.html"
    sess.visit(url)
    source = sess.body()
    soup = BeautifulSoup(source, 'lxml')
    data = get_week_data(soup)
    return data


def get_today_schedule(identifier):
    today = datetime.date.today()
    url = f"{BASE_URL}/data/rasp/{identifier}/view.html?date={today.strftime('%d.%m.%Y')}"
    html = get_html(url)
    soup = BeautifulSoup(html, 'html.parser')
    data = get_current_day_data(soup, today)
    return data


def get_week_data(soup):
    times, schedule = get_schedule_and_times(soup)
    week_schedule = []
    today = datetime.date.today()
    dates = [today + datetime.timedelta(days=i) for i in
             range(0 - today.weekday(), 7 - today.weekday())]
    for i, day in enumerate(schedule):
        lessons = [elem.text.strip() for elem in day]
        week_schedule.append(
            {"date": dates[i],
             "lessons": lessons}
        )
    return {
        "times": times,
        "week_schedule": week_schedule
    }


def get_schedule_and_times(soup):
    table_body = soup.find('tbody')
    times, schedule = parse_rasp_table(table_body)
    parsed_times = [elem.get('title') for elem in times]
    return parsed_times, schedule


def get_current_day_data(soup, date):
    times, schedule = get_schedule_and_times(soup)
    lessons = [elem.text.strip() for elem in schedule[0]]
    return {
        "times": times,
        "day_schedule": {"date": date, "lessons": lessons}
    }


def parse_rasp_table(table_body):
    rows = table_body.find_all('tr')
    times = []
    columns = []
    free_day_indexes = []
    for i, row in enumerate(rows):
        cols = row.find_all('td')
        time = cols.pop(0)
        if i == 0:
            free_day_indexes += [
                idx for idx, element in enumerate(cols) if is_free_day(element)
            ]
            for index in reversed(free_day_indexes):
                del cols[index]
        times.append(time)
        columns.append(cols)
    # transponse schedule to read by line
    schedule = list(map(list, zip(*columns)))
    if free_day_indexes:
        free_day = make_free_day()
        for index in free_day_indexes:
            schedule.insert(index, free_day)
    return times, schedule


def is_free_day(col):
    return "free-day" in col.get("class")


def make_free_day():
    FREE_DAY = '''
    <td class="cell hidden-xs"></td>, 
    <td class="cell hidden-xs"></td>, 
    <td class="cell hidden-xs"></td>, 
    <td class="cell hidden-xs"></td>, 
    <td class="cell hidden-xs"></td>, 
    <td class="cell hidden-xs"></td>, 
    <td class="cell hidden-xs"></td>'''
    soup = BeautifulSoup(FREE_DAY, 'lxml')
    free_day = soup.find_all("td")
    return free_day


def get_html(url):
    r = requests.get(url)
    r.json()
    return r.json().get('html')


if __name__ == '__main__':
    get_current_week_schedule("gruppa_35386")
