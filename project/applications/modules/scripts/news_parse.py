from datetime import datetime as dt
import dryscrape
import requests
from bs4 import BeautifulSoup


def get_html(url):
    r = requests.get(url)
    return r.text


def check_date(item):
    today = dt.today()
    time = item.find("time").get('datetime')
    news_date = dt.strptime(time, "%Y-%m-%d %H:%M:%S")
    return today.date() == news_date.date()


def parse_item(item):
    a = item.find("a")
    href = a.get("href")
    title = a.get('title')
    description = item.find("div", class_="description").text.strip()
    return {
        "href": href,
        "title": title,
        "description": description
    }


def get_data(source):
    soup = BeautifulSoup(source, 'html.parser')
    items = soup.find_all("div", class_="item-body")
    filtered_items = list(filter(check_date, items))
    return [parse_item(item) for item in filtered_items]


def parse_today_news():
    url = "https://news.tpu.ru/news/"
    html = get_html(url)
    data = get_data(html)
    return data


def main():
    url = "https://news.tpu.ru/news/"
    html = get_html(url)
    data = get_data(html)
    print(data)


if __name__ == '__main__':
    main()
