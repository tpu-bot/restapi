from django.contrib import admin

from modules.models import Professor, Department, Group


class ProfessorAdmin(admin.ModelAdmin):
    model = Professor
    # readonly_fields = ('last_name', 'first_name', 'middle_name')
    list_display = ('last_name', 'first_name', 'middle_name', 'mail', "extra_info")
    search_fields = (
        'last_name',
    )


class DepartmentAdmin(admin.ModelAdmin):
    model = Department
    list_display = ('short_name', 'full_name')


class GroupAdmin(admin.ModelAdmin):
    model = Group
    list_display = ('title', 'name', 'department')
    list_filter = ['department']


admin.site.register(Professor, ProfessorAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(Group, GroupAdmin)
