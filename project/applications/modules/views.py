from rest_framework import viewsets, status
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from modules.models import Professor, Group
from modules.scripts.news_parse import parse_today_news
from modules.scripts.parse_rasp import get_current_week_schedule, \
    get_today_schedule
from modules.serializers import ProfessorSerializer, GroupSerializer
from profiles.models import Student, TpuUser, Employee


@api_view()
def schedule_emulation(request):
    lesson_3 = {
        "index": 3,
        "name": "Пркт.Интернет-прил. (ПР)",
        "lector": "Соколова В. В.",
        "location": "к. КЦ, ауд. 418"
    }
    lesson_4 = {
        "index": 4,
        "name": "Пркт.Интернет-прил. (ЛБ)",
        "lector": "Соколова В. В.",
        "location": "к. КЦ, ауд. 418"
    }
    lesson_5 = {
        "index": 5,
        "name": "Польз.интерф.ИП (ПР)",
        "lector": "Саврасов Ф. В.",
        "location": "к. 10, ауд. 113Б"
    }
    first_day = {
        "date": "24.10.2019",
        "lessons": [lesson_3, lesson_4, lesson_5]
    }
    second_day = {
        "date": "25.10.2019",
        "lessons": []
    }
    lesson_3_sat = {
        "index": 3,
        "name": "Администр.в ИС (ЛБ)",
        "lector": "Фадеев А. С.",
        "location": "к. 10, ауд. 109"
    }
    lesson_4_sat = {
        "index": 4,
        "name": "Польз.интерф.ИП (ПР)",
        "lector": "Саврасов Ф. В.",
        "location": "к. КЦ, ауд. 413"
    }
    third_day = {
        "date": "26.10.2019",
        "lessons": [lesson_3_sat, lesson_4_sat]
    }

    rest_of_the_week = [first_day, second_day, third_day]

    return Response({"data": rest_of_the_week})


@api_view()
def get_today_news(request):
    data = parse_today_news()
    return Response({"data": data})


@api_view()
def week_schedule_by_group(request, group_id):
    data = get_current_week_schedule(f"gruppa_{group_id}")
    return Response({"data": data})


@api_view()
def today_schedule_by_group(request, group_id):
    data = get_today_schedule(f"gruppa_{group_id}")
    return Response({"data": data})


def get_identifier_by_user_id(user_id):
    user = TpuUser.objects.filter(id=user_id)
    if not user:
        return None
    occupation = user.last().occupation

    if occupation == TpuUser.STUDENT:
        student = Student.objects.filter(id=user_id)
        if not student:
            return None
        group_id = student[0].group.last().tpu_group_id
        return f"gruppa_{group_id}"

    if occupation == TpuUser.EMPLOYEE:
        employee = Employee.objects.filter(id=user_id)
        if not employee:
            return None
        tpu_user_id = employee.last().professor.tpu_user_id
        return f"user_{tpu_user_id}"

    return None


def get_tpu_user_id_by_professor_id(professor_id):
    professor = Professor.objects.filter(id=professor_id)
    if not professor:
        return None
    tpu_user_id = professor[0].tpu_user_id
    return tpu_user_id


@api_view()
def week_schedule_by_user_id(request, user_id):
    identifier = get_identifier_by_user_id(user_id)
    if not identifier:
        return Response(status=status.HTTP_404_NOT_FOUND)
    data = get_current_week_schedule(identifier)
    return Response({"data": data})


@api_view()
def week_schedule_by_professor_id(request, professor_id):
    user_id = get_tpu_user_id_by_professor_id(professor_id)
    if not user_id:
        return Response(status=status.HTTP_404_NOT_FOUND)
    data = get_current_week_schedule(f"user_{user_id}")
    return Response({"data": data})


@api_view()
def today_schedule_by_user_id(request, user_id):
    identifier = get_identifier_by_user_id(user_id)
    if not identifier:
        return Response(status=status.HTTP_404_NOT_FOUND)
    data = get_today_schedule(identifier)
    return Response({"data": data})


@api_view()
def today_schedule_by_professor_id(request, professor_id):
    user_id = get_tpu_user_id_by_professor_id(professor_id)
    if not user_id:
        return Response(status=status.HTTP_404_NOT_FOUND)
    data = get_today_schedule(f"user_{user_id}")
    return Response({"data": data})




class ProfessorViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Professor.objects.all()
    serializer_class = ProfessorSerializer
    permission_classes = [IsAdminUser, ]

    def filter_queryset(self, queryset):
        last_name = self.request.query_params.get('last_name')
        username = self.request.query_params.get('username')

        if last_name:
            queryset = queryset.filter(last_name__iexact=last_name)

        if username:
            queryset = queryset.filter(personal_site__iendswith=f"/{username}")

        return super().filter_queryset(queryset)


class GroupViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [IsAdminUser, ]

    def filter_queryset(self, queryset):
        name = self.request.query_params.get('name')

        if name:
            queryset = queryset.filter(name__iexact=name)

        return super().filter_queryset(queryset)
