from django.db import models


class Professor(models.Model):
    class Meta:
        verbose_name = "Сотрудник ТПУ"
        verbose_name_plural = "Сотрудники ТПУ"

    first_name = models.CharField(
        max_length=255, verbose_name='Имя'
    )
    middle_name = models.CharField(
        max_length=255, verbose_name='Отчество', blank=True
    )
    last_name = models.CharField(
        max_length=255, verbose_name='Фамилия'
    )
    personal_site = models.CharField(
        max_length=255, verbose_name='Персональный сайт'
    )
    mail = models.CharField(
        max_length=255, verbose_name='Емэйл сотрудника'
    )
    extra_info = models.CharField(
        max_length=255, verbose_name='Дополнительная информация', blank=True
    )
    tpu_user_id = models.CharField(
        max_length=255, verbose_name='Id в системе TPU'
    )

    def __str__(self):
        return f"Сотрудник {self.last_name} {self.first_name}"


class Department(models.Model):
    class Meta:
        verbose_name = "Школа ТПУ"
        verbose_name_plural = "Школы ТПУ"

    short_name = models.CharField(
        max_length=255, verbose_name='Сокращенное название'
    )
    full_name = models.CharField(
        max_length=255, verbose_name='Полное название'
    )

    def __str__(self):
        return f"Школа {self.short_name}"


class Group(models.Model):
    class Meta:
        verbose_name = "Учебная группа в ТПУ"
        verbose_name_plural = "Учебные группы в ТПУ"

    name = models.CharField(
        max_length=255, verbose_name='Номер группы'
    )
    title = models.CharField(
        max_length=255, verbose_name='Направление'
    )
    tpu_group_id = models.CharField(
        max_length=255, verbose_name='Номер группы в системе ТПУ'
    )
    department = models.ForeignKey(
        Department,
        on_delete=models.CASCADE,
        related_name="groups",
        verbose_name='Департамент'
    )

    def __str__(self):
        return self.name
