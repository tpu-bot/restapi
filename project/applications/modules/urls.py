from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import schedule_emulation, ProfessorViewSet, \
    week_schedule_by_group, GroupViewSet, week_schedule_by_user_id, \
    today_schedule_by_user_id, today_schedule_by_group, \
    week_schedule_by_professor_id, today_schedule_by_professor_id, \
    get_today_news

router = DefaultRouter()

router.register(r'professors', ProfessorViewSet, base_name='professors')
router.register(r'groups', GroupViewSet, base_name='group')

urlpatterns = router.urls
urlpatterns += [
    path('schedule/schedule_emulation/', schedule_emulation),
    path('schedule/group/<int:group_id>/current_week/', week_schedule_by_group),
    path('schedule/group/<int:group_id>/today/', today_schedule_by_group),
    path('schedule/user/<int:user_id>/current_week/', week_schedule_by_user_id),
    path('schedule/user/<int:user_id>/today/', today_schedule_by_user_id),
    path('schedule/professor/<int:professor_id>/current_week/', week_schedule_by_professor_id),
    path('schedule/professor/<int:professor_id>/today/', today_schedule_by_professor_id),
    path('news/today/', get_today_news)
]
