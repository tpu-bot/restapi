from rest_framework import serializers

from modules.models import Professor, Group, Department


class ProfessorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Professor
        fields = "__all__"


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        exclude = ['id']


class GroupSerializer(serializers.ModelSerializer):
    department = DepartmentSerializer()

    class Meta:
        model = Group
        fields = "__all__"
