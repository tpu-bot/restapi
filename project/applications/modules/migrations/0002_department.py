# Generated by Django 2.2.6 on 2020-05-25 04:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modules', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('short_name', models.CharField(max_length=255, verbose_name='Сокращенное название')),
                ('full_name', models.CharField(max_length=255, verbose_name='Полное название')),
            ],
            options={
                'verbose_name': 'Школа ТПУ',
                'verbose_name_plural': 'Школы ТПУ',
            },
        ),
    ]
