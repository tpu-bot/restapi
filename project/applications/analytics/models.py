from django.db import models

from profiles.models import TpuUser


class CommandUsage(models.Model):
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name="Дата выполнения"
    )
    tpu_user = models.ForeignKey(
        TpuUser,
        on_delete=models.CASCADE,
        related_name='commands_usages',
        verbose_name='ТПУ пользователь'
    )
    command = models.CharField(
        max_length=100, verbose_name="Команда"
    )

    def __str__(self):
        return f"{self.tpu_user}:{self.command}"

    class Meta:
        verbose_name = "Использование команд"
        verbose_name_plural = "Использования команд"
