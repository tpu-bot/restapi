from django.contrib import admin

from .models import CommandUsage


class CommandUsageAdmin(admin.ModelAdmin):
    model = CommandUsage
    autocomplete_fields = ['tpu_user']
    readonly_fields = ('created_at', 'command')
    list_display = ('created_at', 'command', 'tpu_user')
    list_filter = ['created_at']
    search_fields = ('command',)


admin.site.register(CommandUsage, CommandUsageAdmin)
