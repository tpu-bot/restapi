from rest_framework.routers import DefaultRouter

from analytics.views import CommandUsageViewSet

router = DefaultRouter()


router.register(
    r'command-usage', CommandUsageViewSet,
    base_name='command-usage'
)

urlpatterns = router.urls
