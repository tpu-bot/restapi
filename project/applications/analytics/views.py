from rest_framework import viewsets, permissions

from .models import CommandUsage
from .serializers import CommandUsageSerializer


class CommandUsageViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAdminUser, ]
    queryset = CommandUsage.objects.all()
    serializer_class = CommandUsageSerializer
