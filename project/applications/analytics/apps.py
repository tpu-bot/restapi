from django.apps import AppConfig

from core.messaging import queue


class AnalyticsConfig(AppConfig):
    name = 'analytics'
    verbose_name = 'Аналитика'

    def ready(self):
        connect_result = queue.connect()

        if not connect_result:
            print("Error while connecting to queues: %s" % queue.error)

