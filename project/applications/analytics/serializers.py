from rest_framework import serializers

from .models import CommandUsage


class CommandUsageSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommandUsage
        fields = '__all__'
