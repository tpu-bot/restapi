from django.db import models


class CommandForTelegramBot(models.Model):
    command = models.CharField(
        verbose_name='Команда',
        max_length=100,
    )
    title = models.CharField(
        verbose_name="Название команды",
        max_length=255,
        blank=True, null=True
    )
    text = models.TextField(
        verbose_name='Текст',
        null=False,
        blank=False
    )

    def __str__(self):
        return f"Команда {self.title}"

    class Meta:
        verbose_name = 'Команда для телеграм бота'
        verbose_name_plural = 'Команды для телеграм бота'


class TelegramBotSettings(models.Model):
    registration_hello_message = models.TextField(
        verbose_name="Текст курьеру после регистрации",
        blank=False, null=False
    )

    @classmethod
    def get_actual_settings(cls):
        return cls.objects.order_by('id').last()

    @property
    def is_actual(self):
        actual_settings = TelegramBotSettings.get_actual_settings()
        return actual_settings and actual_settings.id == self.id

    class Meta:
        verbose_name = 'Настройка бота'
        verbose_name_plural = 'Настройки бота'
