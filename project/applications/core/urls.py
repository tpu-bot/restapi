from rest_framework.routers import DefaultRouter

from core.views import TelegramBotSettingsViewSet, CommandForTelegramBotViewSet

router = DefaultRouter()


router.register(
    r'telegram-bot-settings', TelegramBotSettingsViewSet,
    base_name='telegram-bot-settings'
)
router.register(
    r'commands-for-telegram-bot', CommandForTelegramBotViewSet,
    base_name='commands-for-telegram-bot'
)

urlpatterns = router.urls
