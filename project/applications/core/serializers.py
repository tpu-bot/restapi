from rest_framework import serializers

from .models import TelegramBotSettings, CommandForTelegramBot


class TelegramBotSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TelegramBotSettings
        exclude = ['id']


class CommandForTelegramBotSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommandForTelegramBot
        fields = '__all__'
