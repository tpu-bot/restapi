import decimal
import json
import logging

import boto3  # TODO: Add to requirements
from django.conf import settings

logger = logging.getLogger(__name__)

ENABLE_MESSAGING = settings.ENABLE_MESSAGING
AWS_MESSAGING_REGION = settings.AWS_MESSAGING_REGION
SNS_TOPIC_NAME = settings.SNS_TOPIC_NAME

NOTIFICATION_TEST = 'notifications:test'
NEW_MANAGEMENT_BROADCAST = 'global:new_management_broadcast'


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return str(o.real)
        return super(DecimalEncoder, self).default(o)


class MessagesQueue(object):
    def __init__(self):
        self.enabled = ENABLE_MESSAGING
        self.connected = False
        self.error = ''
        self.client = None
        self.topic_name = None

    def connect(self):
        if not self.enabled or self.connected:
            return True

        try:
            self.client = boto3.client('sns', region_name=AWS_MESSAGING_REGION)
            self.topic_name = self._get_topic_name(SNS_TOPIC_NAME)
            self.connected = True
            self.error = ''
            return self.connected
        except StopIteration as ex:
            message = \
                ('Cannot find topic name %s for AWS account in %s region.' +
                 'Create topic with name %s and try again.') % (
                    SNS_TOPIC_NAME, AWS_MESSAGING_REGION, SNS_TOPIC_NAME
                )
            self.error = message
            logger.error(message)
            return False
        except Exception as ex:
            self.error = 'Unknown error while connecting'
            logger.error(ex)
            return False

    def send_message(self, message):
        if not self.enabled or not self.connected:
            return None

        data = json.dumps(message, cls=DecimalEncoder) \
            if type(message) != str else message

        self.client.publish(TopicArn=self.topic_name, Message=data)

    def _get_topic_name(self, name):
        response = self.client.list_topics()
        topic_name = next(
            t['TopicArn'] for t in response['Topics']
            if name in t['TopicArn']
        )

        return topic_name

    def send_test_message(self):
        message = {
            'type': NOTIFICATION_TEST,
            'payload': {
                "message": "Test message"
            }
        }

        self.send_message(json.dumps(message))


queue = MessagesQueue()


def create_new_broadcast_message(broadcast):
    from management.serializers import BroadcastSerializer
    serializer = BroadcastSerializer(broadcast)

    return {
        'type': NEW_MANAGEMENT_BROADCAST,
        'payload': serializer.data
    }