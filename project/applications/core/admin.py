from django.contrib import admin

from .models import CommandForTelegramBot, TelegramBotSettings


class CommandForTelegramBotAdmin(admin.ModelAdmin):
    list_display = ('command', 'text', 'title')


admin.site.register(CommandForTelegramBot, CommandForTelegramBotAdmin)
admin.site.register(TelegramBotSettings)
