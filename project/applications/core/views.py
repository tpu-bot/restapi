from rest_framework import viewsets, permissions
from rest_framework.response import Response

from .models import TelegramBotSettings, CommandForTelegramBot
from .serializers import TelegramBotSettingsSerializer, \
    CommandForTelegramBotSerializer


class TelegramBotSettingsViewSet(
    viewsets.ReadOnlyModelViewSet
):
    permission_classes = [permissions.IsAdminUser]
    serializer_class = TelegramBotSettingsSerializer

    def list(self, request, **kwargs):
        try:
            settings = TelegramBotSettings.get_actual_settings()
        except TelegramBotSettings.DoesNotExist():
            settings = None
            print('Telegram bot settings does not exist')
            # logger.error('Telegram bot settings does not exist')

        data = self.get_serializer(settings).data if settings else {}

        return Response({'results': data})


class CommandForTelegramBotViewSet(
    viewsets.ReadOnlyModelViewSet
):
    queryset = CommandForTelegramBot.objects.all()
    serializer_class = CommandForTelegramBotSerializer
