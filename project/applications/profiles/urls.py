from rest_framework.routers import DefaultRouter

from profiles.views import TpuUserViewSet, StudentViewSet

router = DefaultRouter()

router.register(r'tpu-users', TpuUserViewSet, base_name='tpu-users')
router.register(r'students', StudentViewSet, base_name='students')

urlpatterns = router.urls
