from django.contrib.auth.models import User
from django.db import models

from modules.models import Group, Professor


class TpuUser(models.Model):
    ACCESS_ALLOWED = 'allowed'
    ACCESS_FORBIDDEN = 'forbidden'
    ACCESS_REFUSED = 'refused'
    ACCESS_NOT_REGISTERED_YET = 'not_registered_yet'

    _ACCESS_LEVELS = (
        (ACCESS_ALLOWED, 'Есть доступ'),
        (ACCESS_FORBIDDEN, 'Забанен'),
        (ACCESS_REFUSED, 'Заблокировал бота'),
        (ACCESS_NOT_REGISTERED_YET, 'Пока не зарегистрирован'),
    )

    STUDENT = 'student'
    EMPLOYEE = 'employee'

    _OCCUPATION_CHOICES = (
        (STUDENT, 'Студент'),
        (EMPLOYEE, 'Сотрудник')
    )

    class Meta:
        verbose_name = "Пользователь ТПУ"
        verbose_name_plural = "Пользователи ТПУ"

    def __str__(self):
        return self.tpu_username

    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Время создания'
    )
    user = models.OneToOneField(
        User, on_delete=models.CASCADE,
        related_name='tpu_user', verbose_name='Пользователь'
    )
    tpu_username = models.CharField(
        max_length=255, verbose_name='Никнейм в системе ТПУ', null=True,
    )
    phone = models.CharField(
        max_length=255, verbose_name='Телефон', null=True,
    )
    occupation = models.CharField(
        verbose_name='Род деятельности',
        max_length=100,
        choices=_OCCUPATION_CHOICES,
        default=STUDENT
    )

    status = models.CharField(
        verbose_name='Статус',
        max_length=100,
        choices=_ACCESS_LEVELS,
        default=ACCESS_NOT_REGISTERED_YET
    )


class Student(TpuUser):
    class Meta:
        verbose_name = "Студент ТПУ"
        verbose_name_plural = "Студенты ТПУ"

    group = models.ManyToManyField(
        Group,
        verbose_name='Группа в ТПУ'
    )


class Employee(TpuUser):
    class Meta:
        verbose_name = "Сотрудник ТПУ"
        verbose_name_plural = "Сотрудники ТПУ"

    professor = models.ForeignKey(
        Professor,
        on_delete=models.CASCADE,
        related_name="employee",
        verbose_name="Персональные данные"
    )
    is_confirmed = models.BooleanField(verbose_name="Личность подтверждена")
    receive_students_message = models.BooleanField(
        verbose_name="Получать сообщения от студентов в боте"
    )


class UserTelegramAccount(models.Model):
    class Meta:
        verbose_name = 'Аккаунт пользователя в Telegram'
        verbose_name_plural = 'Аккаунты пользователей в Telegram'

    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Время создания'
    )
    tpu_user = models.OneToOneField(
        TpuUser,
        on_delete=models.CASCADE,
        related_name='telegram_account',
        verbose_name='Курьер'
    )
    chat_id = models.IntegerField(
        verbose_name="ID диалога бота и пользователя в телеграмме",
        unique=True
    )

    def __str__(self):
        return f"Tg acc of {self.tpu_user}"
