# Generated by Django 2.2.6 on 2020-05-26 08:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0004_employee'),
    ]

    operations = [
        migrations.AddField(
            model_name='tpuuser',
            name='occupation',
            field=models.CharField(choices=[('student', 'Студент'), ('employee', 'Сотрудник')], default='student', max_length=100, verbose_name='Род деятельности'),
        ),
    ]
