from django.contrib import admin, auth
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from rest_framework.authtoken.models import Token
from .models import TpuUser, UserTelegramAccount, Student, Employee

User = auth.get_user_model()


class AuthTokenInline(admin.StackedInline):
    model = Token
    extra = 0


class UserTelegramAccountInline(admin.StackedInline):
    model = UserTelegramAccount
    extra = 0


class UserAdmin(BaseUserAdmin):
    inlines = (AuthTokenInline,)


class TpuUserAdmin(admin.ModelAdmin):
    inlines = (UserTelegramAccountInline,)
    search_fields = (
        'id',
        'username',
        'phone',
    )


class StudentAdmin(admin.ModelAdmin):
    inlines = (UserTelegramAccountInline,)
    exclude = ('occupation', )
    autocomplete_fields = ['user']
    search_fields = (
        'id',
        'username',
        'phone',
    )


class EmployeeAdmin(admin.ModelAdmin):
    inlines = (UserTelegramAccountInline,)
    exclude = ('occupation', )
    autocomplete_fields = ['user', 'professor']
    search_fields = (
        'id',
        'username',
        'phone',
    )


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(TpuUser, TpuUserAdmin)
admin.site.register(Student, StudentAdmin)
admin.site.register(Employee, EmployeeAdmin)

