from django.contrib import auth

from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from modules.models import Professor
from .models import TpuUser, UserTelegramAccount, Student, Employee
from .serializers import TpuUserSerializer, StudentSerializer, \
    EmployeeSerializer

User = auth.get_user_model()


class TpuUserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = TpuUser.objects.all()
    permission_classes = [IsAdminUser, ]
    serializer_class = TpuUserSerializer

    def filter_queryset(self, queryset):
        chat_id = self.request.query_params.get('chat_id')

        if chat_id:
            queryset = queryset.filter(telegram_account__chat_id=chat_id)

        return super().filter_queryset(queryset)

    OCCUPATION_MAP = {
        "студент": TpuUser.STUDENT,
        "сотрудник": TpuUser.EMPLOYEE
    }

    @action(
        methods=['post'],
        detail=False,
        permission_classes=[IsAdminUser]
    )
    def register_or_update_tpu_user_from_tg(self, request):
        data = request.data.copy()
        phone = data.get("phone")
        tpu_username = data.get('tpu_username')
        tg_chat_id = data.get("chat_id")
        status = data.get('status')
        data_occupation = data.get('occupation', "студент")
        occupation = self.OCCUPATION_MAP.get(data_occupation)
        group_id = data.get('group_id')
        professor_id = data.get("professor_id")
        get_message_from_student = data.get('get_message_from_student', False)

        user, _ = User.objects.get_or_create(
            username=f"{tpu_username}-{phone}",
        )

        user_data = {
            "tpu_username": tpu_username,
            "phone": phone,
            "status": status,
            "occupation": occupation
        }

        if occupation == TpuUser.EMPLOYEE:
            user_data["receive_students_message"] = get_message_from_student
            user_data["is_confirmed"] = False
            professor = Professor.objects.get(id=professor_id)
            user_data["professor"] = professor
            user_model = Employee
        else:
            user_model = Student

        tpu_user, created = user_model.objects.update_or_create(
            phone=phone,
            defaults={**user_data, "user": user}
        )

        if occupation == TpuUser.STUDENT:
            tpu_user.group.add(group_id)
            tpu_user.save()

        UserTelegramAccount.objects.update_or_create(
            chat_id=tg_chat_id,
            defaults={"tpu_user": tpu_user}
        )

        return Response({
            "success": True,
            "result": TpuUserSerializer(tpu_user).data
        })


class StudentViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Student.objects.all()
    permission_classes = [IsAdminUser, ]
    serializer_class = StudentSerializer

    def filter_queryset(self, queryset):
        chat_id = self.request.query_params.get('chat_id')

        if chat_id:
            queryset = queryset.filter(telegram_account__chat_id=chat_id)

        return super().filter_queryset(queryset)