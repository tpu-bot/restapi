from rest_framework import serializers

from modules.serializers import GroupSerializer, ProfessorSerializer
from .models import TpuUser, UserTelegramAccount, Student, Employee


class UserTelegramAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserTelegramAccount
        exclude = ('tpu_user', 'id')


class TpuUserSerializer(serializers.ModelSerializer):
    telegram_account = UserTelegramAccountSerializer()

    class Meta:
        model = TpuUser
        exclude = ('user',)


class StudentSerializer(serializers.ModelSerializer):
    group = GroupSerializer()
    telegram_account = UserTelegramAccountSerializer()

    class Meta:
        model = Student
        exclude = ('user',)


class EmployeeSerializer(serializers.ModelSerializer):
    professor = ProfessorSerializer()
    telegram_account = UserTelegramAccountSerializer()

    class Meta:
        model = Employee
        exclude = ('user',)