from django.contrib import auth
from django.core.management.base import BaseCommand
from rest_framework.authtoken.models import Token

User = auth.get_user_model()


class Command(BaseCommand):
    help = 'Create user with given token, email and username'

    def add_arguments(self, parser):
        parser.add_argument('token')
        parser.add_argument('email')
        parser.add_argument('username')

    def handle(self, *args, **options):
        token = options['token']
        email = options['email']
        username = options['username']

        token_record = Token.objects.filter(key=token)

        if token_record:
            return

        users = User.objects.filter(username=username)

        if users:
            user = users[0]

            if hasattr(user, 'auth_token'):
                user.auth_token.delete()

            Token.objects.create(key=token, user=user)
        else:
            user = User.objects.create_superuser(username, email, token)
            Token.objects.create(key=token, user=user)
