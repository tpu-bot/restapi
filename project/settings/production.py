# -*- coding: utf-8 -*-
import os

DEBUG = True

# ALLOWED_HOSTS = ["127.0.0.1:8000"]  # must specify domain for production

SECRET_KEY = 'LKasdnj1nJN81NDbf891nJANBgfkb>Ghvahv24'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('DB_NAME'),
        'USER': os.getenv('DB_USER'),
        # 'PASSWORD': os.getenv('DB_PASS'),
        'HOST': os.getenv('DB_HOST'),
        'PORT': os.getenv('DB_PORT'),
    }
}


# Messaging
ENABLE_MESSAGING = True
SNS_TOPIC_NAME = 'tpu-bot'
# SQS_QUEUE_NAME = 'rest'
AWS_MESSAGING_REGION = 'eu-central-1'