FROM python:3.6

ENV DB_NAME=${DB_NAME} \
  DB_USER=${DB_USER} \
  DB_PASS=${DB_PASS} \
  DB_HOST=${DB_HOST} \
  DB_PORT=${DB_PORT}

RUN apt-get update && apt-get install -y \
    qt5-default \
    libqt5webkit5-dev \
    build-essential \
    python-lxml \
    python-pip \
    xvfb

RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/

RUN pip install -r requirements.txt

COPY . /code/
